using System;
using NUnit.Framework;
using TubeBoard;

namespace TubeBoard.Tests
{
	[TestFixture]
	public class OfficialChartLinkGeneratorTests
	{
		[Test]
		[TestCase(1,1985,"1985-01-05")]
		public void GivenAWeekAndYear_WhenCalculated_ThenStartDateIsCorrect(int week, int year, string expected)
		{
			//Arrange
			var sut = new OfficialChartLinkGenerator(week, year);
			//Act
			var start = sut.StartDate.ToString("yyyy-MM-dd");
			//Assert
			Assert.AreEqual(expected, start);
		}

		[Test]
		[TestCase(01,1985,"http://www.officialcharts.com/archive-chart/_/1/1985-01-05/")]
		[TestCase(52,1985,"http://www.officialcharts.com/archive-chart/_/1/1985-12-28/")]
		[TestCase(23,2001,"http://www.officialcharts.com/archive-chart/_/1/2001-06-09/")]
		public void GivenAWeekAndYear_WhenCalculated_ThenUriIsCorrect(int week, int year, string expected)
		{
			//Arrange
			var sut = new OfficialChartLinkGenerator(week, year);
			//Act
			var start = sut.ChartUri.ToString();
			//Assert
			Assert.AreEqual(expected, start);
		}
	}
}

