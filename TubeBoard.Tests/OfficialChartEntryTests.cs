using System;
using NUnit.Framework;
using HtmlAgilityPack;
using TubeBoard;

namespace TubeBoard.Tests
{
	[TestFixture]
	public class OfficialChartEntryTests
	{
		[Test, Ignore]
		public void GivenABrokenHTMLSnippet_WhenCreated_ThenItDoesntCrash()
		{
			Assert.Fail("Not implemented");
		}

		[Test]
		public void GivenASimpleHTMLSnippet_WhenCreated_ThenCorrectDataIsSet()
		{
			//Arrange
			var doc = new HtmlDocument();
			doc.LoadHtml(@"<html><body><table>
			<tr class=""entry"">
              <td class=""currentposition"">2</td>
              <td class=""lastposition"">2<img src=""http://c901052.r52.cf0.rackcdn.com/images-nochange.gif"" alt=""no change""></td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""EVERYTHING SHE WANTS (REMIX)/LAST CHRISTMAS"" class=""coverimage"">
                  <h3>EVERYTHING SHE WANTS (REMIX)/LAST CHRISTMAS</h3>
                  <h4>WHAM!</h4>
                </div>
              </td>
              <td class=""links"">
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"">
                 <h4>EVERYTHING SHE WANTS (REMIX)/LAST CHRISTMAS</h4>
                 WHAM!
                </div>
              </td>
            </tr></table></body></html>");
			var node = doc.DocumentNode.SelectSingleNode("//tr[@class='entry']");

			//Act
			var result = new OfficialChartEntry(node);

			//Assert
			Assert.AreEqual ("EVERYTHING SHE WANTS (REMIX)/LAST CHRISTMAS", result.SongTitle);
			Assert.AreEqual ("WHAM!", result.Band);
			Assert.AreEqual (2, result.CurrentPosition);
			Assert.AreEqual (2, result.LastPosition);
			Assert.AreEqual ("/static/images/occ-chart-thumb.png", result.CoverImage);
			Assert.IsFalse (result.Weeks.HasValue);
		}
	}
}

