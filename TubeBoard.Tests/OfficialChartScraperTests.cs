using System;
using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;
namespace TubeBoard.Tests
{
	[TestFixture]
	public class OfficialChartScraperTests
	{
		[Test]
		public void GivenAnInstance_WhenAValidPageIsUsed_ThenTheDataIsScraped()
		{
			//Arrange
			var sut = new OfficialChartScraper(_input);
			//Act
			var result = sut.Data().ToList();
			//Assert
			Assert.AreEqual(40, result.Count);
		}

		#region input
		private string _input = @"
<!DOCTYPE html>
<html>
<head>
<title>1971-06-26 Top 40 Official UK Singles Archive | Official Charts</title>
<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />
<meta name=""generator"" content=""WickedwebCMS 2.2.0 - http://www.wickedweb.co.uk/"" />
<link rel=""shortcut icon"" href=""http://c901052.r52.cf0.rackcdn.com/favicon.ico""/>
<link rel=""stylesheet"" href=""/static/css/officialcharts.css""/>
<!--[if IE 8]>
	<link rel=""stylesheet"" href=""/static/css/officialcharts-ie8.css""/>
<![endif]-->
<!--[if IE 7]>
	<link rel=""stylesheet"" href=""/static/css/officialcharts-ie7.css""/>
<![endif]-->
<!--[if IE 6]>
	<link rel=""stylesheet"" href=""/static/css/officialcharts-ie6.css""/>
<![endif]-->
<noscript><link rel=""stylesheet"" href=""/static/css/officialcharts-no-js.css""/></noscript>
<script src=""http://aplicaciones.mtvla.com/sitewide/scripts/geo/esi_geo_js.php""></script>
<script src=""http://btg.mtvnservices.com/aria/coda.html?site=officialcharts.com""></script>
</head>
"+@"<body>
<!-- Start of InSkin Ad Code -->
<link href=""http://cdn.inskinmedia.com/isfe/4.1/css/base.css"" rel=""stylesheet"" />
<!--[if lt IE 7]><link href=""http://cdn.inskinmedia.com/isfe/4.1/css/base-ie6.css"" rel=""stylesheet"" /><![endif]-->
<!-- Start of InSkin Container -->
<div id=""InSkinContainer_myPageSkin"" class=""InSkinContainer InSkinAlignCenter"" style=""width: 960px;"">
<!-- Start of Content/Player Container -->
<div id=""InSkinContentContainer_myPageSkin"" class=""InSkinContentContainer"" style=""width: 960px; margin: 0px;"">
<!-- Start of Content/Player Code -->
<div id=""InSkinPageContainer_myPageSkin"" class=""InSkinPageContainer"" style=""background-color:transparent;"">
  <div id=""wrapper"">
    <div id=""wrapInner"">
      <div id=""header"">
        <a href=""/"" id=""logo""><img src=""http://c901052.r52.cf0.rackcdn.com/images-logo.png"" alt=""The Official Charts logo""/></a>
        <div class=""ad leaderboard"">
			<script>
			/* <![CDATA[ */ com.mtvi.ads.AdManager.setKeyValues("""");
			try {
			com.mtvi.ads.AdManager.placeAd({
			size:""728x90"",
			contentType:""adj"",
			event:""null"",
			keyword:""null"",
			});
			} catch(e) {
			};
			/* ]]> */
			</script>
        </div>
"+@"        <div id=""archiveSearch"">
            <h2>Access the archive</h2>
            <form action="""" id=""searchForm"" name=""searchForm"" method=""post"">
				<input type=""hidden"" value=""offcharts_searchform"" name=""ww_form_instance"">
                <ol>
                    <li class=""radios"">
                        <input id=""searchArtist"" name=""type"" type=""radio"" value=""artists"" class=""radio""/>
                        <label for=""searchArtist"" class=""inline"">Artists</label>
  
                        <span id=""archiveTitle"">
                            <input id=""searchTitle"" name=""type"" type=""radio"" value=""albums"" class=""radio""/>
                            <label for=""searchTitle"" class=""inline"">Singles and Album Titles</label>
                        </span>
                    </li>
                    <li id=""searchText"">
                        <input id=""searchInput"" name=""q"" type=""text"" value=""Search by name or title"" class=""textbox""/>
                    </li>
                    <li id=""searchDate"">
                        <span id=""archiveChart"">
                            <input id=""searchChart"" name=""type"" type=""radio"" value=""charts"" class=""radio""/>
                            <label for=""searchChart"" class=""inline"">Charts</label>
                        </span>
                        <select class=""dropdown dateDD"" id=""searchDD"" name=""day"">
                            <option value="""">DD</option>
                            <option value=""01"">01</option>
                            <option value=""02"">02</option>
                            <option value=""03"">03</option>
                            <option value=""04"">04</option>
                            <option value=""05"">05</option>
                            <option value=""06"">06</option>
"+@"                            <option value=""07"">07</option>
                            <option value=""08"">08</option>
                            <option value=""09"">09</option>
                            <option value=""10"">10</option>
                            <option value=""11"">11</option>
                            <option value=""12"">12</option>
                            <option value=""13"">13</option>
                            <option value=""14"">14</option>
                            <option value=""15"">15</option>
                            <option value=""16"">16</option>
                            <option value=""17"">17</option>
                            <option value=""18"">18</option>
                            <option value=""19"">19</option>
                            <option value=""20"">20</option>
                            <option value=""21"">21</option>
                            <option value=""22"">22</option>
                            <option value=""23"">23</option>
                            <option value=""24"">24</option>
                            <option value=""25"">25</option>
                            <option value=""26"">26</option>
                            <option value=""27"">27</option>
                            <option value=""28"">28</option>
                            <option value=""29"">29</option>
                            <option value=""30"">30</option>
                            <option value=""31"">31</option>
                        </select>
                        <select class=""dropdown dateMon"" id=""searchMon"" name=""month"">
                            <option value="""">MM</option>
                            <option value=""01"">Jan</option>
                            <option value=""02"">Feb</option>
"+@"                            <option value=""03"">Mar</option>
                            <option value=""04"">Apr</option>
                            <option value=""05"">May</option>
                            <option value=""06"">Jun</option>
                            <option value=""07"">Jul</option>
                            <option value=""08"">Aug</option>
                            <option value=""09"">Sep</option>
                            <option value=""10"">Oct</option>
                            <option value=""11"">Nov</option>
                            <option value=""12"">Dec</option>
                        </select>
                        <select class=""dropdown dateYYYY"" id=""searchYYYY"" name=""year"">
                            <option value="""">YYYY</option>
                            <option value=""2012"">2012</option>
                            <option value=""2011"">2011</option>
                            <option value=""2010"">2010</option>
                            <option value=""2009"">2009</option>
                            <option value=""2008"">2008</option>
                            <option value=""2007"">2007</option>
                            <option value=""2006"">2006</option>
                            <option value=""2005"">2005</option>
                            <option value=""2004"">2004</option>
                            <option value=""2003"">2003</option>
                            <option value=""2002"">2002</option>
                            <option value=""2001"">2001</option>
                            <option value=""2000"">2000</option>
                            <option value=""1999"">1999</option>
                            <option value=""1998"">1998</option>
                            <option value=""1997"">1997</option>
                            <option value=""1996"">1996</option>
"+@"                            <option value=""1995"">1995</option>
                            <option value=""1994"">1994</option>
                            <option value=""1993"">1993</option>
                            <option value=""1992"">1992</option>
                            <option value=""1991"">1991</option>
                            <option value=""1990"">1990</option>
                            <option value=""1989"">1989</option>
                            <option value=""1988"">1988</option>
                            <option value=""1987"">1987</option>
                            <option value=""1986"">1986</option>
                            <option value=""1985"">1985</option>
                            <option value=""1984"">1984</option>
                            <option value=""1983"">1983</option>
                            <option value=""1982"">1982</option>
                            <option value=""1981"">1981</option>
                            <option value=""1980"">1980</option>
                            <option value=""1979"">1979</option>
                            <option value=""1978"">1978</option>
                            <option value=""1977"">1977</option>
                            <option value=""1976"">1976</option>
                            <option value=""1975"">1975</option>
                            <option value=""1974"">1974</option>
                            <option value=""1973"">1973</option>
                            <option value=""1972"">1972</option>
                            <option value=""1971"">1971</option>
                            <option value=""1970"">1970</option>
                            <option value=""1969"">1969</option>
                            <option value=""1968"">1968</option>
                            <option value=""1967"">1967</option>
                            <option value=""1966"">1966</option>
"+@"                            <option value=""1965"">1965</option>
                            <option value=""1964"">1964</option>
                            <option value=""1963"">1963</option>
                            <option value=""1962"">1962</option>
                            <option value=""1961"">1961</option>
                            <option value=""1960"">1960</option>
                        </select>
                    </li>
                    <li>
                        <input name=""searchSubmit"" type=""submit"" value=""Search"" class=""button""/>	
                    </li>
                </ol>
            </form>
        </div>
              
        <div id=""headerNav"">
            <div id=""fb-root""></div>
            <div class=""fb-like"" data-href=""www.facebook.com/officialcharts"" data-send=""false"" data-layout=""button_count"" data-width=""130"" data-show-faces=""true""></div>
            <a href=""https://twitter.com/officialcharts"" class=""twitter-follow-button"">Follow @officialcharts</a>
        </div>
        <div class=""clear""></div>
      </div>
      <div id=""mainNav"" class=""nav hnav"">
        <ul>
          <li class=""hnav""><a href=""/latest-news/"">News</a>
                  <ul id=""newsNav"">
                      <li><a href=""/latest-news/"" class=""double"">Latest News</a></li>
                      <li><a href=""/features/"" class=""double nav2"">Features</a></li>
                      <li><a href=""/new-releases/"" class=""double nav3"">New Releases</a></li>
                      <li><a href=""/blog/"" class=""double nav4"">Blog</a></li>
"+@"                  </ul>
              </li>
              <li class=""hnav""><a href=""/music-charts/"">Music Charts</a>
                  <ul id=""musicNav"">
                      <li><a href=""/singles-chart/"">Singles Chart</a></li>
                      <li><a href=""/albums-chart/"" class=""nav2"">Albums Chart</a></li>
                      <li><a href=""/compilations-chart/"" class=""nav3"">Compilations Chart</a></li>
                      <li><a href=""/digital-charts/"" class=""nav4"">Digital Charts</a></li>
                      <li><a href=""/classical-charts/"" class=""nav5"">Classical Charts</a></li>
                      <li><a href=""/rock-charts/"" class=""nav6"">Rock Charts</a></li>
                      <li><a href=""/alternative-charts/"" class=""nav7"">Alternative Charts</a></li>
                      <li><a href=""/heritage-charts/"" class=""nav8"">Heritage Charts</a></li>
                      <li><a href=""/urban-charts/"" class=""nav9"">Urban Charts</a></li>
                      <li><a href=""/dance-charts/"" class=""nav10"">Dance Charts</a></li>
                      <li><a href=""/country-charts/"" class=""nav11"">Country Charts</a></li>
                      <li><a href=""/world-charts/"" class=""nav12"">World Chart</a></li>
                      <li><a href=""/asian-charts/"" class=""nav13"">Asian Chart</a></li>
                      <li><a href=""/regional-charts/"" class=""nav14"">Regional Charts</a></li>
                  </ul>
"+@"              </li>
              <li class=""hnav""><a href=""/video-charts/"">Video Charts</a>
                  <ul id=""videoNav"">
                      <li><a href=""/combined-video-chart/"" class=""double"">Combined Video Chart</a></li>
                      <li><a href=""/dvd-chart/"" class=""nav2"">DVD Chart</a></li>
                      <li><a href=""/blu-ray-chart/"" class=""nav3"">Blu-ray Chart</a></li>
                      <li><a href=""/childrens-video-chart/"" class=""nav4"">Children's Video Chart</a></li>
                      <li><a href=""/film-on-video-chart/"" class=""nav5"">Film on Video Chart</a></li>
                      <li><a href=""/music-video-chart/"" class=""nav6"">Music Video Chart</a></li>
                      <li><a href=""/tv-on-video-chart/"" class=""nav7"">TV on Video Chart</a></li>
                      <li><a href=""/sports-and-fitness-video-chart/"" class=""nav8"">Sports &amp; Fitness Video Chart</a></li>
"+@"                      <li><a href=""/special-interest-video-chart/"" class=""nav9"">Special Interest Video Chart</a></li>
                  </ul>
              </li>
              <li class=""hnav""><a href=""/archive/"">Archive</a>
                  <ul id=""archiveNav"">
                      <li><a href=""/archive/music/"" class=""imgLink"">Music Archives<span></span></a></li>
                      <li><a href=""/archive/video/"" class=""imgLink nav2"">Video Archives<span></span></a></li>
                  </ul>
              </li>
              <li class=""hnav""><a href=""/featured-artists/"">Artists</a>
                  <ul id=""artistNav"">
                      <li><a href=""/artist-chart-history-select/_/other/"">#</a></li>
                      <li><a href=""/artist-chart-history-select/_/a/"">A</a></li>
                      <li><a href=""/artist-chart-history-select/_/b/"">B</a></li>
"+@"                      <li><a href=""/artist-chart-history-select/_/c/"">C</a></li>
                      <li><a href=""/artist-chart-history-select/_/d/"">D</a></li>
                      <li><a href=""/artist-chart-history-select/_/e/"">E</a></li>
                      <li><a href=""/artist-chart-history-select/_/f/"">F</a></li>
                      <li><a href=""/artist-chart-history-select/_/g/"">G</a></li>
                      <li><a href=""/artist-chart-history-select/_/h/"">H</a></li>
                      <li><a href=""/artist-chart-history-select/_/i/"">I</a></li>
                      <li><a href=""/artist-chart-history-select/_/j/"">J</a></li>
                      <li><a href=""/artist-chart-history-select/_/k/"">K</a></li>
                      <li><a href=""/artist-chart-history-select/_/l/"">L</a></li>
                      <li><a href=""/artist-chart-history-select/_/m/"">M</a></li>
                      <li><a href=""/artist-chart-history-select/_/n/"">N</a></li>
                      <li><a href=""/artist-chart-history-select/_/o/"">O</a></li>
                      <li><a href=""/artist-chart-history-select/_/p/"">P</a></li>
                      <li><a href=""/artist-chart-history-select/_/q/"">Q</a></li>
                      <li><a href=""/artist-chart-history-select/_/r/"">R</a></li>
                      <li><a href=""/artist-chart-history-select/_/s/"">S</a></li>
                      <li><a href=""/artist-chart-history-select/_/t/"">T</a></li>
                      <li><a href=""/artist-chart-history-select/_/u/"">U</a></li>
                      <li><a href=""/artist-chart-history-select/_/v/"">V</a></li>
                      <li><a href=""/artist-chart-history-select/_/w/"">W</a></li>
                      <li><a href=""/artist-chart-history-select/_/x/"">X</a></li>
                      <li><a href=""/artist-chart-history-select/_/y/"">Y</a></li>
                      <li><a href=""/artist-chart-history-select/_/z/"">Z</a></li>
                  </ul>
              </li>
              
              <li><a href=""http://www.theofficialcharts.com/win/"" class=""off"">Win</a>
              </li>
              
          </ul>
          <span id=""countdown""></span>
          <div class=""clear""></div>
      </div>
      <!-- Breadcrumb -->
      <div id=""breadcrumb"">
          Archive Chart
      </div>
      <!-- END Breadcrumb -->
      <div id=""vault"" class=""box boxPad lightPinkBox layout-fullwidth gap"">
        <div id=""vaultNav"" class=""nav hnav"">
            <ul>
              <li><a class=""on"" href=""/archive/music/"">Music Archive</a></li>
              <li><a href=""/archive/video/"">Video Archive</a></li>
            </ul>
            <div class=""clear""></div>
        </div>
        
  
        <div class=""hgroup col-2-3"">
          <h1><span id=""chartyear"">1971 </span>Top 40 Official UK Singles Archive</h1>
"+@"        </div>
        <div class=""column col-2-3"">
          <div class=""hgroup"">
            <h2 class=""floatLeft"">26th June 1971</h2>
            <form action=""#"" id=""yearSelectForm"">
                <select class=""dropdown"" id=""year"" name=""date"">
                    <option>-- Select a different year --</option>
					<option value=""1960"">1960</option>
					<option value=""1961"">1961</option>
					<option value=""1962"">1962</option>
					<option value=""1963"">1963</option>
					<option value=""1964"">1964</option>
					<option value=""1965"">1965</option>
					<option value=""1966"">1966</option>
					<option value=""1967"">1967</option>
					<option value=""1968"">1968</option>
					<option value=""1969"">1969</option>
					<option value=""1970"">1970</option>
					<option value=""1971"" selected=""selected"">1971</option>
					<option value=""1972"">1972</option>
					<option value=""1973"">1973</option>
					<option value=""1974"">1974</option>
					<option value=""1975"">1975</option>
					<option value=""1976"">1976</option>
					<option value=""1977"">1977</option>
					<option value=""1978"">1978</option>
					<option value=""1979"">1979</option>
					<option value=""1980"">1980</option>
					<option value=""1981"">1981</option>
"+@"					<option value=""1982"">1982</option>
					<option value=""1983"">1983</option>
					<option value=""1984"">1984</option>
					<option value=""1985"">1985</option>
					<option value=""1986"">1986</option>
					<option value=""1987"">1987</option>
					<option value=""1988"">1988</option>
					<option value=""1989"">1989</option>
					<option value=""1990"">1990</option>
					<option value=""1991"">1991</option>
					<option value=""1992"">1992</option>
					<option value=""1993"">1993</option>
					<option value=""1994"">1994</option>
					<option value=""1995"">1995</option>
					<option value=""1996"">1996</option>
					<option value=""1997"">1997</option>
					<option value=""1998"">1998</option>
					<option value=""1999"">1999</option>
					<option value=""2000"">2000</option>
					<option value=""2001"">2001</option>
					<option value=""2002"">2002</option>
					<option value=""2003"">2003</option>
					<option value=""2004"">2004</option>
					<option value=""2005"">2005</option>
					<option value=""2006"">2006</option>
					<option value=""2007"">2007</option>
					<option value=""2008"">2008</option>
					<option value=""2009"">2009</option>
					<option value=""2010"">2010</option>
					<option value=""2011"">2011</option>
					<option value=""2012"">2012</option>
"+@"                </select>
                <input type=""image"" src=""http://c901052.r52.cf0.rackcdn.com/images-go-btn.gif"" alt=""Go"" value=""Go"" class=""buttonInline""/>
            </form>
          </div>
          <table class=""chart purple"">
            <tr>
                <th class=""currentposition""><abbr title=""Chart Position"">Pos</abbr></th>
                <th class=""lastposition""><abbr title=""Chart position Last Week"">LW</abbr></th>
                <th class=""weeks""><abbr title=""Weeks in the chart"">WKs</abbr></th>
                <th class=""info"">Title, Artist</th>
                <th class=""links"">Links</th>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">1</td>
              <td class=""lastposition"">1</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""CHIRPY CHIRPY CHEEP CHEEP"" class=""coverimage"">
                  <h3>CHIRPY CHIRPY CHEEP CHEEP</h3>
                  <h4>MIDDLE OF THE ROAD</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy CHIRPY CHIRPY CHEEP CHEEP"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-4.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.amazon.co.uk/Chirpy-Cheep-Middle-Road/dp/B00004WXZD%3FSubscriptionId%3DAKIAIPUOAFQE7WUA2I7Q%26tag%3Dtheoffcha-21%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00004WXZD"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>CHIRPY CHIRPY CHEEP CHEEP</h4>
                 MIDDLE OF THE ROAD
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">2</td>
              <td class=""lastposition"">3</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/000/558/0000055865_50.jpg"" alt=""I DID WHAT I DID FOR MARIA"" class=""coverimage"">
                  <h3>I DID WHAT I DID FOR MARIA</h3>
                  <h4>TONY CHRISTIE</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy I DID WHAT I DID FOR MARIA"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
"+@"                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/tony-christie/the-tony-christie-love-collection/02-i-did-what-i-did-for-maria/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/000/558/0000055865_50.jpg"" />
                 <h4>I DID WHAT I DID FOR MARIA</h4>
                 TONY CHRISTIE
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">3</td>
              <td class=""lastposition"">4</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""THE BANNER MAN"" class=""coverimage"">
                  <h3>THE BANNER MAN</h3>
                  <h4>BLUE MINK</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>THE BANNER MAN</h4>
                 BLUE MINK
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">4</td>
              <td class=""lastposition"">5</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""I&#039;M GONNA RUN AWAY FROM YOU"" class=""coverimage"">
                  <h3>I&#039;M GONNA RUN AWAY FROM YOU</h3>
                  <h4>TAMI LYNN</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>I&#039;M GONNA RUN AWAY FROM YOU</h4>
"+@"                 TAMI LYNN
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">5</td>
              <td class=""lastposition"">6</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""LADY ROSE"" class=""coverimage"">
                  <h3>LADY ROSE</h3>
                  <h4>MUNGO JERRY</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy LADY ROSE"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-4.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.amazon.co.uk/Lady-Rose-Century-Mungo-Jerry/dp/B00008S0EF%3FSubscriptionId%3DAKIAIPUOAFQE7WUA2I7Q%26tag%3Dtheoffcha-21%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00008S0EF"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>LADY ROSE</h4>
                 MUNGO JERRY
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">6</td>
              <td class=""lastposition"">7</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/000/605/0000060524_50.jpg"" alt=""HE&#039;S GONNA STEP ON YOU AGAIN"" class=""coverimage"">
"+@"                  <h3>HE&#039;S GONNA STEP ON YOU AGAIN</h3>
                  <h4>JOHN KONGOS</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy HE&#039;S GONNA STEP ON YOU AGAIN"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/john-kongos/kongos/09-hes-gonna-step-on-you-again-lp-version/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/000/605/0000060524_50.jpg"" />
                 <h4>HE&#039;S GONNA STEP ON YOU AGAIN</h4>
                 JOHN KONGOS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">7</td>
              <td class=""lastposition"">2</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""KNOCK THREE TIMES"" class=""coverimage"">
                  <h3>KNOCK THREE TIMES</h3>
                  <h4>DAWN</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>KNOCK THREE TIMES</h4>
                 DAWN
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">8</td>
              <td class=""lastposition"">18</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""DON&#039;T LET IT DIE"" class=""coverimage"">
                  <h3>DON&#039;T LET IT DIE</h3>
                  <h4>HURRICANE SMITH</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
"+@"                 <h4>DON&#039;T LET IT DIE</h4>
                 HURRICANE SMITH
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">9</td>
              <td class=""lastposition"">19</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""CO-CO"" class=""coverimage"">
                  <h3>CO-CO</h3>
                  <h4>SWEET</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>CO-CO</h4>
                 SWEET
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">10</td>
              <td class=""lastposition"">9</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""I AM... I SAID"" class=""coverimage"">
                  <h3>I AM... I SAID</h3>
                  <h4>NEIL DIAMOND</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
"+@"                 <h4>I AM... I SAID</h4>
                 NEIL DIAMOND
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">11</td>
              <td class=""lastposition"">8</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""HEAVEN MUST HAVE SENT YOU"" class=""coverimage"">
                  <h3>HEAVEN MUST HAVE SENT YOU</h3>
                  <h4>ELGINS</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>HEAVEN MUST HAVE SENT YOU</h4>
                 ELGINS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">12</td>
              <td class=""lastposition"">13</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""OH YOU PRETTY THING"" class=""coverimage"">
                  <h3>OH YOU PRETTY THING</h3>
"+@"                  <h4>PETER NOONE</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>OH YOU PRETTY THING</h4>
                 PETER NOONE
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">13</td>
              <td class=""lastposition"">17</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/000/157/0000015701_50.jpg"" alt=""JUST MY IMAGINATION (RUNNING AWAY WITH ME)"" class=""coverimage"">
                  <h3>JUST MY IMAGINATION (RUNNING AWAY WITH ME)</h3>
                  <h4>TEMPTATIONS</h4>
                </div>
"+@"              </td>
              <td class=""links"">
                <a title=""Buy JUST MY IMAGINATION (RUNNING AWAY WITH ME)"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/the-temptations/motowns-greatest-hits/11-just-my-imagination-running-away-with-me/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/000/157/0000015701_50.jpg"" />
                 <h4>JUST MY IMAGINATION (RUNNING AWAY WITH ME)</h4>
                 TEMPTATIONS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">14</td>
              <td class=""lastposition"">12</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/002/791/0000279136_50.jpg"" alt=""RAGS TO RICHES"" class=""coverimage"">
                  <h3>RAGS TO RICHES</h3>
                  <h4>ELVIS PRESLEY</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy RAGS TO RICHES"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/elvis-presley-(2)/the-50-greatest-love-songs-1-2/22-rags-to-riches/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/002/791/0000279136_50.jpg"" />
                 <h4>RAGS TO RICHES</h4>
                 ELVIS PRESLEY
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">15</td>
              <td class=""lastposition"">10</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""INDIANA WANTS ME"" class=""coverimage"">
                  <h3>INDIANA WANTS ME</h3>
                  <h4>R. DEAN TAYLOR</h4>
                </div>
              </td>
              <td class=""links"">
                
"+@"                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>INDIANA WANTS ME</h4>
                 R. DEAN TAYLOR
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">16</td>
              <td class=""lastposition"">22</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""I DON&#039;T BLAME YOU AT ALL"" class=""coverimage"">
                  <h3>I DON&#039;T BLAME YOU AT ALL</h3>
                  <h4>SMOKEY ROBINSON AND THE MIRACLES</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>I DON&#039;T BLAME YOU AT ALL</h4>
                 SMOKEY ROBINSON AND THE MIRACLES
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">17</td>
              <td class=""lastposition"">11</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/003/584/0000358456_50.jpg"" alt=""MY BROTHER JAKE"" class=""coverimage"">
                  <h3>MY BROTHER JAKE</h3>
"+@"                  <h4>FREE</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy MY BROTHER JAKE"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/free/free-hit-pac-5-series/04-my-brother-jake/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/003/584/0000358456_50.jpg"" />
                 <h4>MY BROTHER JAKE</h4>
                 FREE
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">18</td>
              <td class=""lastposition"">16</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""BROWN SUGAR/BITCH/LET IT ROCK"" class=""coverimage"">
                  <h3>BROWN SUGAR/BITCH/LET IT ROCK</h3>
                  <h4>ROLLING STONES</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>BROWN SUGAR/BITCH/LET IT ROCK</h4>
                 ROLLING STONES
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">19</td>
              <td class=""lastposition"">14</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""MALT AND BARLEY BLUES"" class=""coverimage"">
                  <h3>MALT AND BARLEY BLUES</h3>
                  <h4>MCGUINNESS FLINT</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>MALT AND BARLEY BLUES</h4>
                 MCGUINNESS FLINT
                </div>
              </td>
            </tr>
"+@"            <tr class=""entry"">
              <td class=""currentposition"">20</td>
              <td class=""lastposition"">15</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/002/935/0000293591_50.jpg"" alt=""I THINK OF YOU"" class=""coverimage"">
                  <h3>I THINK OF YOU</h3>
                  <h4>PERRY COMO</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy I THINK OF YOU"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/perry-como-(2)/the-love-collection-vol-1/15-i-think-of-you/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/002/935/0000293591_50.jpg"" />
                 <h4>I THINK OF YOU</h4>
                 PERRY COMO
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">21</td>
              <td class=""lastposition"">27</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""PIED PIPER"" class=""coverimage"">
"+@"                  <h3>PIED PIPER</h3>
                  <h4>BOB AND MARCIA</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>PIED PIPER</h4>
                 BOB AND MARCIA
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">22</td>
              <td class=""lastposition"">20</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""MOZART SYMPHONY NO. 40 IN G MINOR"" class=""coverimage"">
                  <h3>MOZART SYMPHONY NO. 40 IN G MINOR</h3>
                  <h4>WALDO DE LOS RIOS</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>MOZART SYMPHONY NO. 40 IN G MINOR</h4>
                 WALDO DE LOS RIOS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">23</td>
              <td class=""lastposition"">23</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""LAZY BONES"" class=""coverimage"">
                  <h3>LAZY BONES</h3>
                  <h4>JONATHAN KING</h4>
                </div>
              </td>
              <td class=""links"">
                
"+@"                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>LAZY BONES</h4>
                 JONATHAN KING
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">24</td>
              <td class=""lastposition"">26</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""JOY TO THE WORLD"" class=""coverimage"">
                  <h3>JOY TO THE WORLD</h3>
                  <h4>THREE DOG NIGHT</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy JOY TO THE WORLD"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-4.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.amazon.co.uk/Joy-World-Three-Dog-Night/dp/B00006B0JL%3FSubscriptionId%3DAKIAIPUOAFQE7WUA2I7Q%26tag%3Dtheoffcha-21%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00006B0JL"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>JOY TO THE WORLD</h4>
                 THREE DOG NIGHT
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">25</td>
              <td class=""lastposition"">21</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""JIG-A-JIG"" class=""coverimage"">
                  <h3>JIG-A-JIG</h3>
                  <h4>EAST OF EDEN</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>JIG-A-JIG</h4>
                 EAST OF EDEN
                </div>
              </td>
            </tr>
            <tr class=""entry"">
"+@"              <td class=""currentposition"">26</td>
              <td class=""lastposition"">32</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""WHEN YOU ARE A KING"" class=""coverimage"">
                  <h3>WHEN YOU ARE A KING</h3>
                  <h4>WHITE PLAINS</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>WHEN YOU ARE A KING</h4>
                 WHITE PLAINS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">27</td>
              <td class=""lastposition"">30</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""DOUBLE BARREL"" class=""coverimage"">
                  <h3>DOUBLE BARREL</h3>
                  <h4>DAVE AND ANSEL COLLINS</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>DOUBLE BARREL</h4>
                 DAVE AND ANSEL COLLINS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
"+@"              <td class=""currentposition"">28</td>
              <td class=""lastposition"">24</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/000/012/0000001231_50.jpg"" alt=""HEY WILLY"" class=""coverimage"">
                  <h3>HEY WILLY</h3>
                  <h4>HOLLIES</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy HEY WILLY"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/the-hollies/the-air-that-i-breathe-the-very-best-of-the-hollies/23-hey-willy/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/000/012/0000001231_50.jpg"" />
                 <h4>HEY WILLY</h4>
                 HOLLIES
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">29</td>
              <td class=""lastposition"">29</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""IT&#039;S A SIN TO TELL A LIE"" class=""coverimage"">
                  <h3>IT&#039;S A SIN TO TELL A LIE</h3>
                  <h4>GERRY MONROE</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>IT&#039;S A SIN TO TELL A LIE</h4>
                 GERRY MONROE
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">30</td>
              <td class=""lastposition"">NEW</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""BLACK AND WHITE"" class=""coverimage"">
                  <h3>BLACK AND WHITE</h3>
                  <h4>GREYHOUND</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>BLACK AND WHITE</h4>
                 GREYHOUND
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">31</td>
              <td class=""lastposition"">NEW</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""(AND THE) PICTURES IN THE SKY"" class=""coverimage"">
                  <h3>(AND THE) PICTURES IN THE SKY</h3>
                  <h4>MEDICINE HEAD</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>(AND THE) PICTURES IN THE SKY</h4>
                 MEDICINE HEAD
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">32</td>
              <td class=""lastposition"">NEW</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""ME AND YOU AND A DOG NAMED BOO"" class=""coverimage"">
                  <h3>ME AND YOU AND A DOG NAMED BOO</h3>
                  <h4>LOBO</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>ME AND YOU AND A DOG NAMED BOO</h4>
                 LOBO
                </div>
              </td>
"+@"            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">33</td>
              <td class=""lastposition"">NEW</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""RIVER DEEP - MOUNTAIN HIGH"" class=""coverimage"">
                  <h3>RIVER DEEP - MOUNTAIN HIGH</h3>
                  <h4>SUPREMES AND THE FOUR TOPS</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>RIVER DEEP - MOUNTAIN HIGH</h4>
                 SUPREMES AND THE FOUR TOPS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">34</td>
              <td class=""lastposition"">33</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""SUGAR SUGAR"" class=""coverimage"">
                  <h3>SUGAR SUGAR</h3>
                  <h4>SAKKARIN</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>SUGAR SUGAR</h4>
                 SAKKARIN
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">35</td>
              <td class=""lastposition"">28</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""UN BANC, UN ARBRE, UNE RUE"" class=""coverimage"">
                  <h3>UN BANC, UN ARBRE, UNE RUE</h3>
                  <h4>SEVERINE</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>UN BANC, UN ARBRE, UNE RUE</h4>
                 SEVERINE
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">36</td>
              <td class=""lastposition"">35</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/002/903/0000290321_50.jpg"" alt=""MY WAY"" class=""coverimage"">
                  <h3>MY WAY</h3>
                  <h4>FRANK SINATRA</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy MY WAY"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/frank-sinatra-(2)/frank-sinatra/220-my-way/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-4.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.amazon.co.uk/My-Way-Frank-Sinatra/dp/B00000APSC%3FSubscriptionId%3DAKIAIPUOAFQE7WUA2I7Q%26tag%3Dtheoffcha-21%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00000APSC"" class=""affiliatebuy"">Buy</a>
                  </div>
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/002/903/0000290321_50.jpg"" />
                 <h4>MY WAY</h4>
                 FRANK SINATRA
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">37</td>
              <td class=""lastposition"">RE</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""http://cdn.7static.com/static/img/sleeveart/00/002/828/0000282861_50.jpg"" alt=""(WHERE DO I BEGIN) LOVE STORY"" class=""coverimage"">
                  <h3>(WHERE DO I BEGIN) LOVE STORY</h3>
                  <h4>ANDY WILLIAMS</h4>
                </div>
              </td>
              <td class=""links"">
                <a title=""Buy (WHERE DO I BEGIN) LOVE STORY"" href=""#"" class=""buysong"">Buy</a>
                <div class=""affiliates"">
                  <div class=""affiliate"">
                  <img src=""http://c901052.r52.cf0.rackcdn.com/images-affiliates-1.jpg"" alt="""" class=""affiliateimg"">
                  <a target=""_blank"" href=""http://www.7digital.com/artists/andy-williams/greatest-hits-volume-ii/01-where-do-i-begin-love-story/?partner=777"" class=""affiliatebuy"">Buy</a>
                  </div>
"+@"                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""http://cdn.7static.com/static/img/sleeveart/00/002/828/0000282861_50.jpg"" />
                 <h4>(WHERE DO I BEGIN) LOVE STORY</h4>
                 ANDY WILLIAMS
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">38</td>
              <td class=""lastposition"">37</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""ROSE GARDEN"" class=""coverimage"">
                  <h3>ROSE GARDEN</h3>
                  <h4>LYNN ANDERSON</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>ROSE GARDEN</h4>
                 LYNN ANDERSON
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">39</td>
              <td class=""lastposition"">NEW</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""IF YOU COULD READ MY MIND"" class=""coverimage"">
                  <h3>IF YOU COULD READ MY MIND</h3>
                  <h4>GORDON LIGHTFOOT</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>IF YOU COULD READ MY MIND</h4>
                 GORDON LIGHTFOOT
                </div>
              </td>
            </tr>
            <tr class=""entry"">
              <td class=""currentposition"">40</td>
              <td class=""lastposition"">NEW</td>
              <td class=""weeks""></td>
              <td class=""info"">
                <div class=""infoHolder"">
                  <img src=""/static/images/occ-chart-thumb.png"" alt=""LEAP UP AND DOWN"" class=""coverimage"">
                  <h3>LEAP UP AND DOWN</h3>
                  <h4>ST. CECILIA</h4>
                </div>
              </td>
              <td class=""links"">
                
                <div class=""affiliates"">
                </div>
                <div class=""previewwindow"">
                 <img class=""coverimage"" src=""/static/images/occ-chart-thumb.png"" />
                 <h4>LEAP UP AND DOWN</h4>
                 ST. CECILIA
                </div>
              </td>
            </tr>
          </table>
          <!-- AddThis Button BEGIN -->
          <div class=""addthis_toolbox addthis_default_style "">
              <a class=""addthis_button_facebook_like"" fb:like:layout=""button_count""></a>
              <a class=""addthis_button_tweet""></a>
              <a class=""addthis_button_google_plusone"" g:plusone:size=""medium""></a>
              <a href=""/send-to-a-friend/"" class=""sendTo""></a>
          </div>
          <!-- AddThis Button END -->
        </div>
  
        <div id=""rightCol"" class=""aside"">
          <div class=""ad mpu adgap"">
			<script>
			/* <![CDATA[ */ com.mtvi.ads.AdManager.setKeyValues("""");
			try {
			com.mtvi.ads.AdManager.placeAd({
			size:""300x250"",
			contentType:""adj"",
			event:""null"",
"+@"			keyword:""null"",
			});
			} catch(e) {
			};
			/* ]]> */
			</script>
              <span>Advertisement</span>
          </div>
          <div id=""twitter"" class=""box boxPad lightBlueBox lightBlue gap"">
              <h2 class=""title"">Follow us on Twitter</h2>
          <!--<a href="""" id=""myRetweet"" class=""retweet"" target=""new"">retweet</a>-->
              <div id=""latest_tweet"">
                  <span class=""entry-meta loading""></span>
                  <div class=""clear""></div>
              </div>
              <a href=""http://twitter.com/OfficialCharts"" title=""Follow @OfficialCharts on Twitter"" rel=""external"" class=""morelink"">Follow us on Twitter</a>
				<div class=""fb-like-box"" data-href=""http://www.facebook.com/OfficialCharts"" data-width=""270"" data-height=""367"" data-show-faces=""true"" data-stream=""false"" data-header=""true""></div>
				<div class=""clear""></div>
          </div>
          <div class=""box pinkBox pink gap"">
            <div>
              <a href=""/iphone-applications/"" class=""cta"">
                <img src=""http://c0903002.cdn.cloudfiles.rackspacecloud.com/2624-callout-app-2.jpg"" alt=""iphone apps"" />
              </a>
              <a href=""/archive/"" class=""cta"">
                <img src=""http://c0903002.cdn.cloudfiles.rackspacecloud.com/2623-callout-archive-2.jpg"" alt=""chart archives"" />
              </a>
            </div>
          </div>
        </div>
        <div class=""clear""></div>
      </div>
          
      <div id=""footer"" class=""box pinkBox layout-fullwidth"">
          <div id=""quickNav"" class=""column col-2-3 nav"">
              <h2>About Us</h2>
              <ul>
                <li><h3>Who We Are</h3>
                  <ul>
                    
                      <li><a href=""http://www.officialcharts.com/theofficialchartscompany/"">The Official Charts Company</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/history-of-the-official-charts-percy-dickins-tribute/"">The People Who Made The Charts</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/the-charts-we-compile/"">The Charts We Compile</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/where-the-charts-appear/"">Where The Charts Appear</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/faqs/"">FAQs</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/copyright-notice/"">Copyright Notice</a></li>
                      
                  </ul>
                </li>
              </ul>
              <ul>
                  <li><h3>B2B Services</h3>
                  <ul>
                    
                      <li><a href=""http://www.officialcharts.com/getting-releases-into-the-charts/"">Getting Releases Into The Charts</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/register-releases/"">Register Releases</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/chart-rules/"">Chart Rules</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/register-for-bpi-certificates/"">Register For BPI Certificates</a></li>
"+@"                      
                      <li><a href=""http://www.officialcharts.com/becoming-a-chart-retailer/"">Becoming A Chart Retailer</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/data-tracking/"">Data Tracking</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/industry-data/"">Industry Data</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/topline/"">Topline</a></li>
                      
                      <li><a href=""http://www.officialcharts.com/industry-reports/"">Industry Reports</a></li>
                      
                  </ul>
                  </li>
              </ul>
              <ul class=""endcolumn"">
                <li><a href=""http://www.officialcharts.com/press-area/"">Press Area</a></li>
                
                <li><a href=""http://www.officialcharts.com/contacts/"">Contacts</a></li>
                
                <li><a href=""http://www.officialcharts.com/our-partners/"">Our Partners</a></li>
                
              </ul>
          </div>
          <div class=""column endcolumn col-1-3"">
              <div id=""socialNav"" class=""nav hnav"">
                  <h2>Connect with Official Charts</h2>
                  <ul>
                    <li><a href=""http://www.facebook.com/OfficialCharts"" class=""imgLink"" id=""icoFacebook"" target=""_blank"">Facebook<span></span></a></li>
                    <li><a href=""http://twitter.com/OfficialCharts"" class=""imgLink"" id=""icoTwitter"" target=""_blank"">Twitter<span></span></a></li>
                    <li><a href=""http://www.youtube.com/OfficialCharts"" class=""imgLink"" id=""icoYoutube"" target=""_blank"">YouTube<span></span></a></li>
                    <li><a href=""http://www.muzu.tv/officialcharts"" class=""imgLink"" id=""icoMuzu"" target=""_blank"">Muzu<span></span></a></li>
                  </ul>
              </div>
              <div id=""signUp"">
                  <h2>Sign up for the latest chart news</h2>
                  <form action=""/newsletter-signup/"" name=""newsSignupForm"" id=""newsSignupForm"">
                      <input type=""text"" name=""newsSignupEmail"" id=""newsSignupEmail"" class=""textbox"" value=""Your email""/>
                      <input id=""newsSignupSubmit"" type=""submit"" value="""" class=""button""/>
                  </form>
              </div>
          </div>
          <div class=""clear""></div>
          <div id=""footerNav"" class=""nav hnav"">
              <ul>
                  <li><a href=""http://www.officialcharts.com/sitemap/"" title=""Sitemap"">Sitemap</a></li><li><a href=""http://www.officialcharts.com/privacy-policy/"" title=""Privacy Policy"">Privacy Policy</a></li><li><a href=""http://www.officialcharts.com/cookie-policy/"" title=""Cookie Policy"">Cookie Policy</a></li><li><a href=""http://www.officialcharts.com/terms-and-conditions/"" title=""Terms &amp; Conditions"">Terms &amp; Conditions</a></li><li><a href=""http://www.officialcharts.com/accessibility/"" title=""Accessibility"">Accessibility</a></li>
              </ul>
          </div>
          <span id=""copyright"">&copy; The Official UK Charts Company 2012 | <a href=""http://www.wickedweb.co.uk/"" id=""wwlink"" target=""_blank"">Digital Marketing Agency</a> - <a href=""http://www.wickedweb.co.uk/solutions/"" target=""_blank"">Wickedweb</a></span>
          <div class=""clear""></div>
      </div>
    </div>
  </div>
</div>
<!-- End of Content/Player Code -->
</div>
<!-- End of Content/Player Container -->
</div>
<script src=""http://c901052.r52.cf0.rackcdn.com/javascript-jquery-1.4.4.min.js""></script>
<script src=""/static/javascript/officialcharts.js""></script>
<script src=""http://cdn.inskinmedia.com/isfe/4.1/js/base/api/pageskin.js""></script>
<script>
InSkinParams['myPageSkin'] = {
		'srv_SectionID': '124121',
		'srv_Keywords': location.pathname.split('/')[1],
		'srv_UseSAS': 'true',
		'sas_PauseAdTagURL':'http://ad.doubleclick.net/adj/theofficialcharts.com/pageskin;sec0='+location.pathname.split('/')[1]+';sec1='+location.pathname.split('/')[2]+';tile=6;sz=2x2;ord='+Math.random()*10000000000000000,
		'plr_ContentType': 'PAGESKIN',
		'plr_ContentID': 'myContent',
		'plr_ContentW': 960,		
		'plr_FrameTop': 90,
		'plr_FrameSide': 130,
		'plr_FrameBottom': 90,
		'plr_HideElementsByID': '',
		'plr_HideElementsByClass': 'leaderboard,mpu',
		'plr_NoSkinInSkinResize': true,
		'plr_GetParamsFromMeta': ['srv_Keywords', 'keywords']
"+@"	};
	var objPageSkin=new InSkin.Base(""myPageSkin"");
  objPageSkin.addEventListener(objPageSkin.EVENT_AD_SERVED,function () {
      InSkin.$('body').css('background','none')
  })
  </script>
  
  <!-- End of InSkin Configuration -->
  <!-- Start of InSkin Initialization -->
  <script>objPageSkin.init();</script>
  <!-- End of InSkin Initialization -->
  <!-- End of InSkin Ad Code -->
<script src=""http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f5a27491435de9f""></script>
<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) {return;}js = d.createElement(s); js.id = id;js.src = ""//connect.facebook.net/en_US/all.js#xfbml=1"";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
<script src=""//platform.twitter.com/widgets.js""></script>
<script type=""text/javascript"">
var gaJsHost = ((""https:"" == document.location.protocol) ? ""https://ssl."" : ""http://www."");
document.write(unescape(""%3Cscript src='"" + gaJsHost + ""google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E""));
</script>
<script type=""text/javascript"">
try {
var pageTracker = _gat._getTracker(""UA-11584073-1"");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>";
		#endregion
	}
}
