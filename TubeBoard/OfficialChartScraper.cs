using System;
using System.Collections.Generic;
using System.Linq;

using HtmlAgilityPack;

namespace TubeBoard
{
	public class OfficialChartScraper
	{
		HtmlDocument _doc = new HtmlDocument();

		public OfficialChartScraper (string input)
		{
			_doc.LoadHtml(input);
		}

		public OfficialChartScraper (Uri sourceUri)
		{
			var webGet = new HtmlWeb();
			_doc = webGet.Load(sourceUri.ToString());
		}

		public IEnumerable<OfficialChartEntry> Data()
		{
			var nodes = _doc.DocumentNode.SelectNodes("//tr[@class='entry']");
			return nodes.Select(x=>new OfficialChartEntry(x));
		}
	}
}

