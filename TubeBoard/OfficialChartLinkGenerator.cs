using System;

namespace TubeBoard
{
	public class OfficialChartLinkGenerator
	{
		public OfficialChartLinkGenerator (int week, int year)
		{
			StartDate = DateTimeUtils.FirstDateOfWeek(year, ++week).AddDays(-2);
			ChartUri = new Uri(string.Format("http://www.officialcharts.com/archive-chart/_/1/{0:yyyy-MM-dd}/", StartDate));
		}

		public DateTime StartDate {
			get;
			set;
		}

		public Uri ChartUri
		{
			get; set;
		}
	}
}

