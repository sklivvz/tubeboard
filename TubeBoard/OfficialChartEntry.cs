using System;
using HtmlAgilityPack;

namespace TubeBoard
{
	public class OfficialChartEntry
	{
		public OfficialChartEntry(HtmlNode source)
		{
			if (source == null) throw new ArgumentNullException("source");
			SongTitle = source.SelectSingleNode(@"./td[@class='info']/div[@class='infoHolder']/h3").InnerText;
			Band = source.SelectSingleNode(@"./td[@class='info']/div[@class='infoHolder']/h4").InnerText;
			CurrentPosition = TryExtractNumber(source, "./td[@class='currentposition']").Value;
			LastPosition = TryExtractNumber(source, "./td[@class='lastposition']");
			Weeks = TryExtractNumber(source, "./td[@class='weeks']");
			CoverImage = source.SelectSingleNode("./td[@class='links']/div[@class='previewwindow']/img[@class='coverimage']").Attributes["src"].Value;
		}

		private int? TryExtractNumber(HtmlNode source, string xpath)
		{
			var val = source.SelectSingleNode(xpath).InnerText;
			int res;
			return int.TryParse(val, out res)?res:(int?)null;
		}

		public string SongTitle { 
			get; 
			internal set;
		}

		public string Band {
			get;
			internal set;
		}

		public int CurrentPosition {
			get;
			internal set;
		}

		public int? LastPosition {
			get;
			internal set;
		}

		public int? Weeks {
			get;
			internal set;
		}

		public string CoverImage {
			get;
			internal set;
		}
	}
}

