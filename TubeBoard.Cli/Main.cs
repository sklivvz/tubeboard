using System;
using System.Linq;

namespace TubeBoard.Cli
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int y = int.Parse(args[0]);
			int w = int.Parse(args[1]);
			var uri = new OfficialChartLinkGenerator(w, y).ChartUri;
			var chart = new OfficialChartScraper(uri).Data();
			foreach(OfficialChartEntry entry in chart)
			{
				Console.WriteLine("{2} Artist: {0}\tSong: {1}", entry.Band, entry.SongTitle, entry.CurrentPosition);
			}
		}
	}
}
